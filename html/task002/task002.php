<?php
//List of cities
$gradovi = array("Tokyo", "Mexico City","New York City", "Mumbai", "Seoul", "Shanghai", "Lagos", "Buenos Aires", "Cairo", "London");

//The number of cities in the list
$broj = count($gradovi);

//Going through the list
for($i = 0; $i <  $broj; $i++) 
{
	//Display cities
     echo $gradovi[$i];
	//New line and display commas after name of cities
     echo ",<br>";
}
//New line
echo "<br>";

//Sort cities
sort($gradovi);

$br = count($gradovi);
for($j = 0; $j <  $br; $j++) 
{
     echo $gradovi[$j];
     echo ",<br>";
}

//New line
echo "<br>";

//Add to array list
array_push($gradovi, "Los Angeles", "Calcutta", "Osaka", "Beijing");

//Sort cities
sort($gradovi);

//The number of cities in the list
$b = count($gradovi);

//Going through the list
for($n = 0; $n <  $b; $n++) 
{
	//Display cities
     echo $gradovi[$n];
	//New line and display commas after name of cities
     echo ",<br>";
}
//New line
echo "<br>";

?>

