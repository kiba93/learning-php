<?php
	$browsers=array("None","Firefox","Chrome","Internet Explorer","Safari","Opera","Other");
	
	$speeds=array("Unknown","DSL","T1","Cable","Dialup","Other");

	$os=array("Windows","Linux","Macintosh","Other");
	
	class Select
	{
		private $name;
		private $value;
		
		public function setName($name)
		{
			$this->name = $name;
		}
		
		public function getName()
		{
			return $this->name;
		}
		
		public function setValue($value)
		{
			$this->value = $value;
		}
		
		public function getValue()
		{
			return $this->value;
		}
		
		private function makeOptions($value)
		{
			foreach($value as $v)
			{
				echo "<option value=\"$v\">" .ucfirst($v). "</option>\n";
			}
		}

		public function makeSelect()
		{
			echo "<select name=\"" .$this->getName(). "\">\n";
			$this->makeOptions($this->getValue());
			echo "</select>" ;
		}
	}
?>

<h2>Registration<br/></h2>

<?php
	if(!isset($_POST['submit']))
	{
		?>
		<form method="post" action="task-14.php">
		<p>Name:<br/>
		<input type="text" name="name" size="50"/></p>
		<p>Username:<br/>
		<input type="text" name="username" size="50"/></p>
		<p>Email:<br/>
		<input type="text" name="email" size="50"/></p>
		
		<p><strong>Work Access</strong></p>
		<p>Primary Browser:

		<?php
		$browserWork = new Select();
		$browserWork->setName('browserWork');
		$browserWork->setValue($browsers);
		$browserWork->makeSelect();
		unset($browserWork);
		
		echo "</p>\n<p>Connection Speed: ";
		$speedWork = new Select();
		$speedWork->setName('speedWork');
		$speedWork->setValue($speeds);
		$speedWork->makeSelect();
		unset($speedWork);
		
		echo "</p>\n<p>Operating System: ";
		$osWork = new Select();
		$osWork->setName('osWork');
		$osWork->setValue($os);
		$osWork->makeSelect();
		unset($osWork);
		
		?>
		</p>
	
		<p><strong>Home Access</strong></p>
		<p>Primary Browser:
		
		<?php
		$browserHome = new Select();
		$browserHome->setName('browserHome');
		$browserHome->setValue($browsers);
		$browserHome->makeSelect();
		unset($browserHome);
		
		echo "</p>\n<p>Connection Speed: ";
		$speedHome = new Select();
		$speedHome->setName('speedHome');
		$speedHome->setValue($speeds);
		$speedHome->makeSelect();
		unset($speedHome);
		
		echo "</p>\n<p>Operating System: ";
		$osHome = new Select();
		$osHome->setName('osHome');
		$osHome->setValue($os);
		$osHome->makeSelect();
		unset($osHome);
		?>
		</p>
	<p/>
	<input type="submit" name="submit" value="Go" />
	</form>
	
	<?php
	}
	else
	{
		$name=$_POST['name'];
		$username=$_POST['username'];
		$email=$_POST['email'];
		$browserWork=$_POST['browserWork'];
		$speedWork=$_POST['speedWork'];
		$osWork=$_POST['osWork'];
		$browserHome=$_POST['browserHome'];
		$speedHome=$_POST['speedHome'];
		$osHome=$_POST['osHome'];
		
		if (empty($name))
		{
			die('Error: Please enter your name. <br/>
			<input type="submit" name="back" value="Back"
			onclick="self.location=\'task-14.php\'" /></body></html> ');
		}
		
		if (empty($username))
		{
			die('Error: Please choose a username. <br/>
			<input type="submit" name="back" value="Back"
			onclick="self.location=\'task-14.php\'" /> </body></html> ');
		}
		
		$char = strpos($email, '@');
		
		if (empty($email) || $char === false )
		{
			die('Error: Please enter a valid email address. <br />
			<input type="submit" name="back" value="Back"
			onclick="self.location=\'task-14.php\'" /> </body></html> ');
		}
		
		echo "<p>The following data has been saved for $name: </p>\n";
	echo "<p>Username: $username<br />\n";
	echo "Email: $email</p>\n";
	echo "<p>Work Access:</p>\n";
	echo "<ul>\n<li>$browserWork</li>\n";
	echo "<li>$speedWork</li>\n";
	echo "<li>$osWork</li>\n</ul>\n";
	echo "<p>Home Access:</p>\n";
	echo "<ul>\n<li>$browserHome</li>\n";
	echo "<li>$speedHome</li>\n";
	echo "<li>$osHome</li>\n</ul>\n";
	}
?>
