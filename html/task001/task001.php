<?php

// Initially need to enter a user name
echo "Please enter your name: " . PHP_EOL;

// Waiting for the user to enter something and press enter
$name = read_stdin();

// Enter your email
echo "Please enter your email: " . PHP_EOL;

// Waiting for the user to enter email
$email = read_stdin();

// This will display the thank you message including their name they entered.
echo "Thank you $name." . PHP_EOL;
echo "We will send our promotions to your email: $email" . PHP_EOL;

// our function to read from the command line
function read_stdin()
{
        $fr=fopen("php://stdin","r");   // open our file pointer to read from s$
        $input = fgets($fr,128);        // read a maximum of 128 characters
        $input = rtrim($input);         // trim any trailing spaces.
        fclose ($fr);                   // close the file handle
        return $input;                  // return the text entered
}

?>
