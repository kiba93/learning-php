<?php
//List of cities, states and continents
$multiCity = array(
    array("City", "Country", "Continent"),
    array("Tokyo", "Japan", "Asia"),
    array("Mexico City","Mexico", "North America"),
    array("New York City", "USA", "North America"),
    array("Mumbai", "India", "Asia"),
    array("Seoul", "Korea", "Asia"),
    array("Shanghai", "China", "Asia"),
    array("Lagos", "Nigeria", "Africa"),
    array("Buenos Aires", "Argentina", "South America"),
    array("Cairo", "Egypt", "Africa"),
    array("London", "UK","Europe")
);
?>

<head>
<style type="text/css">
td, th {width: 8em; border: 1px solid black; padding-left: 4px;}
th {text-align:center;}
table {border-collapse: collapse; border: 1px solid black;}
</style>
</head>

<table>
<thead>
<tr>
<th>
<?php
//Taking the first value in the list and appointment of a table caption
	echo $multiCity[0][0] ."</th>\n<th>";
	echo $multiCity[0][1] ."</th>\n<th>";
	echo $multiCity[0][2] ."</th>\n";
	?>
</tr>
</thead>

<?php
//Counts the number of rows in a list
$br = count($multiCity);

//Start from the first row to the list
for ($row=1; $row<$br; $row++)
{
	//In the other rows adds value by going through the list
	echo "<tr>\n";
	//Takes an entire row for value
	foreach ($multiCity[$row] as $value)
	{
		//Put the value in the rows
		echo "<td>$value</td>\n";
	}
	echo "</tr>\n";
}
?>
