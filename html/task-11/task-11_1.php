<?php
$BrowersTypes = array("Firefox","Chrome","Internet Explorer","Safari","Opera","Other");

class Select
{
	private $name;
        private $value;

	public function setName($name)
  	{
  		$this->name = $name;
	}

	public function getName()
	{
  		return $this->name;
	}

	public function setValue ($value)
	{
  		$this->value = $value;
	}

	public function getValue()
	{
		return $this->value;
	}

	private function makeOptions($value)
	{
  		foreach($value as $v)
  		{
			echo "<option value=\"$v\">" .ucfirst($v). "</option>\n";
  		}
	}

	public function makeSelect()
	{
  		echo "<select name=\"" .$this->getName(). "\">\n";
  		$this->makeOptions($this->getValue());
  		echo "</select>";
	}
}
?>

<h2>Registration<br/></h2>

<?php
	if(!isset($_POST['submit']))
  	{
	?>
   		<form method="post" action="task-11_1.php">
		<p>First and last name:<br/>
		<input type="text" name="name" size="50"/></p>
		<p>Username:<br/>
		<input type="text" name="username" size="50"/></p>
		<p>Email:<br/>
		<input type="text" name="email" size="50"/></p>

		<p>Browser:
		<?php
		$browser = new Select();
		$browser->setName('browser');
		$browser->setValue($BrowersTypes);
		$browser->makeSelect();
		?>
		</p>
		<input type="submit" name="submit" value="Go" />
		</form>
		<?php
	}

	else
	{
		$name=$_POST['name'];
    		$username=$_POST['username'];
    		$email=$_POST['email'];
    		$selBrowsersTypes=$_POST['BrowerssTypes'];

		echo "The following data has been saved for $name: <br/>";
    		echo "Username: $username<br/>";
    		echo "Email: $email<br/>";
    		echo "Browser: $selBrowsersTypes<br/>";
	}
?>
